When calling modpol.lua from another file, one may wish to use 
different functions than those included by default. 

So far, 2 definitions need to be made BEFORE calling modpol.lua, the rest can be overwritten
after calling modpol.lua .

setup:
----------------
Define the global modpol table:
modpol = {}

the following 2 items *may* be defined, if they are not, then defaults will be used:
========================
--------------------
modpol.get_script_dir()
--------------------
type: function

defaults to: a function that can only work in linux

usage: must return the file path of the modpol directory. If modpol is in a
larger directory, such as a mod filesystem, make sure that the path returned is the "base" 
modpol folder, i.e. the folder with all the original modpol files.
========================
--------------------
modpol.storage_file_path
--------------------
type: string

defaults to: topdir .. "/storage/storage-local.lua", 
where topdir is the directory defined by modpol.storage_file_path 

usage: if you wish to use another method of storage than that provided by default, 
then you can save a filepath to the storage script here. 

