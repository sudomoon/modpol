modpol.modules = modpol.modules or {}

modpol.modules.consent = {}

-- sets consent to its own callback
modpol.modules.consent.__index = modpol.modules.consent

function temp_consent_process()
    return {
        type = "consent",
        id = nil,
        org_id = nil,
        request_id = nil,
        total_votes = 0,
        majority_to_pass = 0.51,
        votes_needed = nil,
        votes_yes = {},
        votes_no = {}
    }
end

-- ===============================================
-- function to create a new consent process to resolve a pending process
function modpol.modules.consent:new_process(id, request_id, org_id)
    local process = temp_consent_process()
    process.id = id
    process.request_id = request_id
    process.org_id = org_id

    setmetatable(process, modpol.modules.consent)
    modpol.ocutil.log('Created new process #' .. id .. ' for request id #' .. request_id)

    local p_org = modpol.orgs.get_org(org_id)

    for i, member in ipairs(p_org.members) do
        p_org:add_pending_action(id, member)
    end

    process.votes_needed = math.ceil(process.majority_to_pass * p_org:get_member_count())

    return process
end

-- ============================
-- interact function for the consent module
-- input: user (string)
function modpol.modules.consent:interact(user)
   -- TODO this needs more context on the vote at hand
   modpol.interactions.binary_poll_user(
      user, "Do you consent?",
      function(vote)
         if vote == 'Yes' then
            self:approve(user, true)
         elseif vote == 'No' then
            self:approve(user, false)
         end
   end)
end

-- ======================================================
-- function for users to vote on a pending request
function modpol.modules.consent:approve(user, decision)
   if not modpol.orgs.get_org(self.org_id):has_member(user) then
       modpol.ocutil.log('Error in consent:approve -> user not a member of the org')
       return
    end

    if decision then
        table.insert(self.votes_yes, user)
        modpol.ocutil.log('User ' .. user .. ' voted yes on request #' .. self.request_id)
    else
        table.insert(self.votes_no, user)
        modpol.ocutil.log('User ' .. user .. ' voted no on request #' .. self.request_id)
    end

    self.total_votes = self.total_votes + 1
    
    local p_org = modpol.orgs.get_org(self.org_id)
    p_org:remove_pending_action(self.id, user)

    self:update_status()
end

-- ===================================================
-- determines whether process has finished and resolves request if it has (unfinished)
function modpol.modules.consent:update_status()
    local process_org = modpol.orgs.get_org(self.org_id)
    
    if #self.votes_yes >= self.votes_needed then
        modpol.ocutil.log('Request #' .. self.request_id .. ' passes')
        process_org:resolve_request(self.request_id, true)
    elseif #self.votes_no >= self.votes_needed then
        modpol.ocutil.log('Request #' .. self.request_id .. ' fails to pass')
        process_org:resolve_request(self.request_id, false)
    else
        modpol.ocutil.log('Waiting for more votes...')
    end
end
