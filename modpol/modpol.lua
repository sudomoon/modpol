-- ===================================================================
-- /modpol.lua
-- Modular Politics (modpol) core

-- ===================================================================
-- Basic tables

-- Main API table
if not modpol then modpol = {} end

-- Record of every state change should appear here
modpol.ledger = {
}


-- ===================================================================
-- Locate framework top-level directory.

-- This function is intended for use under Linux and/or UNIX only.  

-- It
-- returns  a relative or  absolute path  for the framework  top-level
-- directory without a trailing slash.

-- if your application has a different method of getting the script directory, 
-- then feel free to overwrite this in an init.lua or other such file by first
-- defining the modpol table and then defining the get_script_dir() function

local get_script_dir = modpol.get_script_dir or function() 
    local  str = debug.getinfo (2, "S").source:sub (2)
           str = str:match ("(.*/)") or "."
           str = str:gsub ("/$", "", 1)
    return str
end

-- Absolute or relative path to script directory.
local  topdir = get_script_dir()
modpol.topdir = topdir
print (topdir)

-- ===================================================================
-- Load dependencies

-- OldCoder utilities
dofile (topdir .. "/util/ocutil/ocutil.lua")

-- ===================================================================
-- Persistent storage
-- must implement modpol.load_storage() and modpol.store_data()


-- Select a storage method
-- -- preferably, declare this in the init.lua that calls modpol.lua  This is the default.
-- Works with CLI:
modpol.storage_file_path = modpol.storage_file_path or topdir .. "/storage/storage-local.lua"
-- Works with Minetest 5:
--modpol.storage_file_path = modpol.storage_file_path or topdir .. "/storage/storage-mod_storage.lua")

--execute the storage file
dofile (modpol.storage_file_path)


-- If available, load persistent storage into active tables
modpol.load_storage()

-- ===================================================================
-- ModPol core features

dofile (topdir .. "/api.lua")


-- ===================================================================
-- Final checks

-- sets org metatable on load
if (modpol.orgs.array) then
    for id, org in ipairs(modpol.orgs.array) do
        if type(org) == 'table' then 
            setmetatable(org, modpol.orgs)
            -- sets process metatable on load
            for id, process in ipairs(org.processes) do
                if type(process) == 'table' then
                    setmetatable(process, modpol.modules[process.type])
               end
            end
        end
    end
end

-- create instance if not present
modpol.instance = modpol.orgs.array[1] or modpol.orgs.init_instance()


modpol.ocutil.log ("modpol loaded")

-- ===================================================================
-- End of file.
