dofile("../modpol.lua")

print('\nRemoving existing orgs')
modpol.orgs.reset()

print('\nCreating an org called "test_org"')
test_org = modpol.instance:add_org('test_org', 'luke')

print('\nTrying to create an org with the same name')
duplicate = modpol.instance:add_org('test_org', 'luke')

print('\nAdding user "nathan" to test_org')
test_org:add_member('nathan')

print('\nTrying to add duplicate user to test_org')
test_org:add_member('nathan')

print('\nRemoving user "nathan" from test_org')
test_org:remove_member('nathan')

print('\nTrying to remove user "nathan" from empty member list')
test_org:remove_member('nathan')

