dofile("../modpol.lua")

print('\nRemoving existing orgs')
modpol.orgs.reset()

print('\nCreating an org called "test_org"')
test_org = modpol.instance:add_org('test_org', 'luke')

print('\nAdding user "nathan" to test_org')
test_org:add_member('nathan')

print("\nOkay, let's start with requests. Setting up an org...")
modpol.instance:add_org('test_org', 'luke')
test_org:add_member('nathan')

print('\nMaking consent the add_member policy')
test_org:set_policy("add_member", "consent", false);

print('\nMaking a request to add Josh')
add_josh = {
    user = "josh",
    type = "add_member",
    params = {"josh"}
}
request_id = test_org:make_request(add_josh)


print('\nHave the two members vote on it')
modpol.interactions.org_dashboard("nathan","test_org")
modpol.interactions.org_dashboard("luke","test_org")

