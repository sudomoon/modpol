dofile('../modpol.lua');

modpol.orgs.reset()

test_org = modpol.instance:add_org('test_org', 'lukvmil')
test_org:add_member('luke')
test_org:add_member('nathan')

test_org:set_policy("add_member", "consent", false);

new_request = {
    user = "josh",
    type = "add_member",
    params = {"josh"}
}

request_id = test_org:make_request(new_request)

-- process_id = test_org:create_process("consent", request_id)
for id, process in ipairs(test_org.processes) do
    process:approve('luke', true)
    process:approve('nathan', true)
end
-- process = test_org.processes[process_id]
-- process:approve("luke", true)
-- process:approve("nathan", true)

modpol.instance:set_policy("add_org", "consent", false);

new_request = {
    user = "luke",
    type = "add_org",
    params = {"new_org"}
}


modpol.instance:add_member('luke')
modpol.instance:add_member('josh')
modpol.instance:add_member('nathan')

request_id = modpol.instance:make_request(new_request)
modpol.instance:make_request({
    user="luke",
    type="add_org",
    params={"second_org"}
})

modpol.interactions.login()

for id, process in ipairs(modpol.instance.processes) do
    -- process:approve('luke', true)
    process:approve('josh', true)
end
