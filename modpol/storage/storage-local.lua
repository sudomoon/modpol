-- ===================================================================
-- /storage-local.lua
-- Persistent storage in /data using Serpent Serializer
-- Unix systems only

-- ===================================================================
-- Set directories and filenames

modpol.datadir     = modpol.topdir  .. "/data"
modpol.file_ledger = modpol.datadir .. "/ledger.dat"
modpol.file_orgs   = modpol.datadir .. "/orgs.dat"
modpol.file_old_ledgers   = modpol.datadir .. "/old_ledgers.dat"

os.execute ("mkdir -p " .. modpol.datadir)

modpol.ocutil.setlogdir  (modpol.datadir)
modpol.ocutil.setlogname ("modpol.log")

-- ===================================================================
-- Set up the Serpent Serializer functions.

modpol.serpent = {}
dofile (modpol.topdir .. "/util/serpent/serpent.lua")

-- ===================================================================
-- This function stores "ledger" data to disk.

local store_ledger = function(verbose)
    local ok = modpol.ocutil.file_write (modpol.file_ledger, modpol.serpent.dump (modpol.ledger))

    if ok ~= true then
        modpol.ocutil.fatal_error ("store_data: ledger")
    end

    local nn = modpol.ocutil.table_length (modpol.ledger)
    local str = "entries"
    if nn == 1 then str = "entry" end
    if verbose then modpol.ocutil.log (nn .. " global ledger entries stored to disk") end
end

-- ===================================================================
-- This function stores "orgs" data to disk.

local store_orgs = function()
    local ok =  modpol.ocutil.file_write (modpol.file_orgs,
    modpol.serpent.dump (modpol.orgs))
    if    ok ~= true then
        modpol.ocutil.fatal_error ("store_data: orgs")
    end

    local nn  = modpol.ocutil.table_length (modpol.orgs.array)
    local str = "entries"
    if nn == 1 then str = "entry" end
    if verbose then modpol.ocutil.log (nn .. " orgs stored to disk") end
end


-- ===================================================================
-- This function stores data to disk.

modpol.store_data = function(verbose)
    store_ledger(verbose)
    store_orgs(verbose)
end

-- ===================================================================
-- This function loads "ledger" data from disk.

local load_ledger = function()
    local obj =  modpol.ocutil.file_read (modpol.file_ledger )
    if    obj ~= nil then
        local func, err = load (obj)
        if err then
            modpol.ocutil.fatal_error ("load_data: ledger"   )
        end
        modpol.ledger = func()

        local nn  = modpol.ocutil.table_length (modpol.ledger)
        local str = "entries"
        if nn == 1 then str = "entry" end
        modpol.ocutil.log (nn .. " global ledger entries loaded from disk")
    else
        modpol.ocutil.log ("No stored global ledger data found")
    end
end

-- ===================================================================
-- This function loads "orgs" data from disk.

local load_orgs   = function()
    local obj =  modpol.ocutil.file_read (modpol.file_orgs )
    if    obj ~= nil then
        local func, err = load (obj)
        if err then
            modpol.ocutil.fatal_error ("load_data: orgs"   )
        end
        modpol.orgs = func()

        -- this block resets the metatable after being loaded in so that the class functions work
        -- for id, org in ipairs(modpol.orgs.array) do
        --     setmetatable(org, modpol.orgs)
        -- end

        local nn  = modpol.ocutil.table_length (modpol.orgs.array)
        local str = "entries"
        if nn == 1 then str = "entry" end
        modpol.ocutil.log (nn .. " orgs loaded from disk")
    else
        modpol.ocutil.log ("No stored orgs data found")
    end
end

-- ===================================================================
-- This function loads "old_ledgers" data from disk.

local load_old_ledgers   = function()
    local obj =  modpol.ocutil.file_read (modpol.file_old_ledgers )
    if    obj ~= nil then
        local func, err = load (obj)
        if err then
            modpol.ocutil.fatal_error ("load_data: old_ledgers"   )
        end
        modpol.old_ledgers = func()
 
        local nn  = modpol.ocutil.table_length (modpol.old_ledgers)
        local str = "entries"
        if nn == 1 then str = "entry" end
        modpol.ocutil.log (nn .. " old ledgers loaded from disk")
    else
        modpol.ocutil.log ("No stored old ledgers data found")
    end
end

-- ===================================================================
-- This function loads stored data from disk.

modpol.load_storage = function()
    load_ledger()
    load_orgs()
    -- load_old_ledgers()
end

-- ===================================================================
-- End of file.
