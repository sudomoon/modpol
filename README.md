# Modular Politics Prototype for Minetest

This is a mod for [Minetest](https://minetest.net) that enables diverse governance mechanisms. It seeks to implement the [Modular Politics](https://metagov.org/modpol) proposal. In the future, it
will be possible to use this framework to simulate governance in a
number of platform contexts.

This mod produces an API that can serve as a dependency for other mods that add specific governance functionalities. 

For background information and project roadmap, see [the wiki](https://gitlab.com/medlabboulder/modpol/-/wikis/home).

## Installation in Minetest

To use this in Minetest, simply install it in your mods/ or worldmods/ folder. Minetest will load init.lua.

In the game, open the Modular Politics interface with the command `/modpol`.


## Standalone Version on the Command Line

Modular Politics can also be used independently of Minetest as a command-line tool. Currently command-line use of modpol requires a Unix-style system, but it is intended to become more fully platform independent.

The command-line version is in the `modpol` subdirectory. To interact with the interpreter on Unix systems in CLI mode, install lua or luajit and execute the following command in this directory:

```
$ cd modpol/
$ lua [or luajit]
> dofile("modpol.lua")
```

For a list of global functions and tables, use `modpol.menu()`.

## Storage

By default, a  data  directory  named  "data" will  be created in  this directory. "/data" will contain a log file and serialized program data files.

Another storage method may be chosen in modpol.lua. A StorageRef-based method for Minetest 5.* is included: storage-mod_storage.lua.


## Credits

Initiated by [Nathan Schneider](https://nathanschneider.info) of the [Media Enterprise Design Lab](https://colorado.edu/lab/medlab) at the University of Colorado Boulder, as part of the [Metagovernance Project](https://metagov.org). Based on the paper "[Modular Politics: Toward a Governance Layer for Online Communities](https://metagov.org/modpol)."

Other contributors include:

* [Luke Miller](https://gitlab.com/lukvmil) (main control flow, object orientation, module spec)
* [MisterE](https://gitlab.com/gbrrudmin) (project refactoring, core feature development)
* Robert Kiraly [[OldCoder](https://github.com/oldcoder/)] (ocutils.lua, storage-local.lua, project refactoring)

We'd love to welcome more contributors, particularly from the Minetest community! Please join the conversation in the [Issues](https://gitlab.com/medlabboulder/modpol/-/issues) or the [Minetest.net forum](https://forum.minetest.net/viewtopic.php?f=47&t=26037).

We are grateful for support for this project from a residency with [The Bentway Conservancy](https://www.thebentway.ca/).

## Licenses

* [Project](LICENSE.mt): MIT
* [Lua Serpent Serializer](serpent/LICENSE.txt): MIT
