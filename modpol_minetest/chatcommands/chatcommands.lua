-- ===================================================================
-- Minetest commands
-- ===================================================================

command_list = {}               -- user-facing table of commands

local chat_table                -- MT chat command definitions table
local regchat                   -- Chat-command registration function

regchat = minetest.register_chatcommand

regchat = function(name, command_table)
   minetest.register_chatcommand(name, command_table)
   table.insert(command_list, name)
end

-- ===================================================================
-- /modpol
-- Presents a menu of options to users
regchat(
   "modpol", {
      privs = {},
      func = function(user)
         modpol.interactions.dashboard(user)
      end,
})

-- ===================================================================
-- /reset
-- For testing only
-- Clears the system and recreates instance with all players
regchat(
   "reset", {
      privs = {},
      func = function(user)
         modpol.orgs.reset();
         return true, "Reset orgs"
      end,
})



-- ===================================================================
-- /addorg
-- This code defines a chat command which creates a new
-- "org". Presently, the command makes the user the sole member of the
-- "org".

regchat(
   "addorg", {
      privs        = {} ,
      func         = function (user, param)
         local success, message = modpol.instance:add_org (param)
         return true, message
      end        
})

-- ===================================================================
-- /listorgs
-- In Minetest mode, this code defines a chat command which lists
-- existing "orgs".
-- The list shows one "org" per line in the following format:
-- org_name (member, member, ...)

regchat(
   "listorgs", {
      privs = {} ,
      func  = function (user, param)
         return true, "Orgs: " ..
            table.concat(modpol.orgs.list_all(), ", ")
      end
})

-- ===================================================================
-- /listplayers
regchat(
   "listplayers", {
      privs = {},
      func = function(user)
         local result = table.concat(modpol.list_users(),", ")
         return true, "All players: " .. result
      end,
})

-- ===================================================================
-- /joinorg
regchat(
   "joinorg", {
      privs = {},
      func = function(user, param)
         local org = modpol.orgs.get_org(param)
         local success, result = org:add_member(user)
         return true, result
      end,
})


-- ===================================================================
-- /pollself [question]
-- asks the user a question specified in param
regchat(
   "pollself", {
      privs = {},
      func = function(user, param)
         modpol.interactions.binary_poll_user(user, param)
         return true, result
      end,
})
