modpol = {}

-- ===================================================================
--preoverrides: certain things must be predefined in the global table for them to have effect (or cmd line version defualts are used)
-- ===================================================================
-- ===================================================================
-- currently:
-- --get_script_dir: get modpol_minetest's version of directory 
-- -- Persistent storage
-- -- -- must implement modpol.load_storage() and modpol.store_data()
-- -- -- defines the path for the lua file



-- get modpol_minetest's version of directory
modpol.get_script_dir = function()
   return minetest.get_modpath("modpol").."/modpol"
end

-- TKTK: Implement minetest settingtypes for this... for now, the default is to use minetest mod storage. 
-- However, the default for if modpol.lua is called from the cmd line is to use local storage
-- Any other implementation may with to make their own persistent storage file and declare it befoe calling modpol.lua in a similar manner

-- works with cmd line: "/storage/storage-local.lua", works with Minetest 5.0 and up: "/storage/storage-mod_storage.lua"

modpol.storage_file_path = minetest.get_modpath("modpol").."/modpol_minetest/storage/storage-mod_storage.lua"

-- ===================================================================
-- /init.lua
-- Modular Politics (modpol) for Minetest
-- TKTK Maybe make this just a quick ref file and locate MT files elsewhere?
-- TKTK need to add player to orgs.instance with on_joinplayer

-- ===================================================================
-- Load modpol system

dofile(minetest.get_modpath("modpol") .. "/modpol/modpol.lua")


-- ===================================================================
-- Modular Politics Minetest Specific Code
-- ===================================================================
dofile(minetest.get_modpath("modpol") .. "/modpol_minetest/api.lua")

-- ===================================================================
-- End of file.
